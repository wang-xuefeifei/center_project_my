import request from '@/utils/request'

/**
 * @description: 登录
 * @param {*} data {modile, password}
 * @return {*}
 */
export function login (data) {
  // return axios请求的Promise对象
  return request({
    url: '/sys/login',
    method: 'post',
    data
  })
}

// 登录人的接口
export const getUserInfo = () => {
  return request({
    url: '/sys/profile',
    method: 'post'
  })
}

// 用户的信息
export function getUserId (id) {
  return request({
    url: `/sys/user/${id}`
  })
}

/**
 * @description: 为用户分配角色
 * @param {*} data { id:当前用户id, roleIds:选中的角色id组成的数组 }
 * @return {*}
 */
export function assignRoles (data) {
  return request({
    url: '/sys/user/assignRoles',
    data,
    method: 'put'
  })
}

