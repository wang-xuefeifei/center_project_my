// 注册为全剧组件
import PageTools from '@/components/PageTools'
import UploadExcel from './UploadExcel'
import TreeDepyart from './treeDeptart/index.vue'
// 头像上传
import UploadPhoto from './UploadPhoto/index.vue'
// 二维码
import Qrcode from './Qrcode/index.vue'

const components = [PageTools, UploadExcel, TreeDepyart, UploadPhoto, Qrcode]

const pluging = {
  install (Vue) {
    components.forEach((opo) => {
      Vue.component(opo.name, opo)
    })
    /* // PageTools组件   'PageTools'组件名称
    Vue.component('PageTools', PageTools) */
  }
}
export default pluging
