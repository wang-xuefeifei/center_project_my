// 路由实例   还有动态路由asyncRoutes
import router, { asyncRoutes } from './router'
// vuex
import store from './store'
// import { Message } from 'element-ui'
import NProgress from 'nprogress' // progress bar
import 'nprogress/nprogress.css' // progress bar style

// NProgress.configure({ showSpinner: false }) // NProgress Configuration

// 白名单=> 不需要token也可以访问的页面
const whiteList = ['/login', '/404'] // no redirect whitelist
// 前门保安
/**
 * to 去哪
 * from 从哪来
 * next 是否放行
 */
router.beforeEach(async (to, from, next) => {
  // start progress bar
  NProgress.start()
  if (store.getters.token) {
    // 存在token
    if (to.path === '/login') {
      // 如果再次访问登录页=》回到首页
      next('/')
    } else {
      // 让他先跳  因为访问页面是响应式的  所以他可以先执行
      next()
      // 登录人信息：
      // 1.有token  2.访问页面之前
      if (!store.getters.name) {
        // 因为他是异步的  401 出错   会阻塞后边的代码
        const res = await store.dispatch('user/getUserInfon')
        // 后续业务处理数据
        /**
         * 上一步是获取登录人的基本信息
         * 需求 根据过去的标识过滤动态路由 用户只能访问指定的页面
         * 1.获取标识
         * 2.根据登录人的权限表示数据过滤动态路由
         */
        console.log(888, res)
        // 过滤动态路由
        const canlook = asyncRoutes.filter(route => {
          return res.menus.includes(route.children[0].name)
        })
        // 存储菜单的数据
        store.commit('menus/setMenusList', canlook)
        console.log(7774, canlook)
        // 动态添加路由配置
        router.addRoutes([...canlook, { path: '*', redirect: '/404', hidden: true }])
      }
      // 放行
      // next()
    }
  } else {
    //  不存在token=>可能访问的是登录页
    // includes 数组的方法
    if (whiteList.includes(to.path)) {
      next()
    } else {
      // 携带当前要访问的页面路径  带参数
      next(`/login?redirect=${to.path}`)
    }
  }
}
)

// 后门保安
router.afterEach(() => {
  // finish progress bar
  // 进度条关闭
  NProgress.done()
})
