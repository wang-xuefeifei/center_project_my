const getters = {
  sidebar: state => state.app.sidebar,
  device: state => state.app.device,
  // 用户相关数据
  // token
  token: state => state.user.token,
  // 头像
  avatar: state => state.user.userInfo.staffPhoto,
  // 用户名
  name: state => state.user.userInfo.username,
  // 工号
  workNumber: state => state.user.userInfo.workNumber,
  // s手机号
  mobile: state => state.user.userInfo.mobile,
  // 部门
  departmentName: state => state.user.userInfo.departmentName
}
export default getters
