/**
 * 存储系统token和登录人信息
 */
// 导入token持久化方法
// 导入登录接口
import { login, getUserInfo, getUserId } from '@/api/user'
import { setToken, getToken, removeToken } from '@/utils/auth'
// 导入路由重置
import { resetRouter } from '@/router/index'
export default {
  // 避免冲突命名空间
  namespaced: true,
  // 存数据
  state: {
    // token
    // 先从本地cookie读取=》如果上次存了=》赋值
    token: getToken() || null,
    // 登录人的信息
    userInfo: {}
  },
  // 修改数据
  mutations: {
    // 存储token
    setToken (state, token) {
      // 只是存到内存
      state.token = token
      // 持久化=》本地存一份(本地硬盘)
      setToken(token)
    },
    // 删除token
    delToken (state) {
      state.token = null
      removeToken()
    },
    // 登录人信息
    setUserInfo (statr, userInfo) {
      statr.userInfo = userInfo
    },
    // 删除登录人信息
    devUserInfo (state) {
      state.userInfo = {}
    }
  },
  // 调用后台接口
  actions: {
    /**
     * 登录获取token并存储
     * @param {*} ctx 上下文
     * @param {*} loginForm 页面传递参数
     */
    async login (ctx, loginForm) {
      // 调用登录接口
      const res = await login(loginForm)
      // console.table(res)
      // 调用mutation方法setToken存储token
      ctx.commit('setToken', res)
    },
    // 登录人信息
    async getUserInfon (ctx) {
      // 到接口那登录人的信息
      const userinfoo = await getUserInfo()
      console.log(userinfoo)
      // 登录人的头像
      const userpoprt = await getUserId(userinfoo.userId)
      console.log(userpoprt)
      // 把异步拿到的登录人的信息传到mutations里
      ctx.commit('setUserInfo', { ...userinfoo, ...userpoprt })
      return userinfoo.roles
    },
    // 退出登录
    logout (ctx) {
      // 删除登录人的信息
      ctx.commit('devUserInfo')
      // 删除token
      ctx.commit('delToken')
      // 1. 重置路由和清空菜单数据
      resetRouter()
      // 2. 重置菜单数据
      // 子模块如果需要调用兄弟模块的mutations方法，需要在第三个参数添加{ root: true }
      ctx.commit('menus/setMenusList', [], { root: true })
    }
  }
}
