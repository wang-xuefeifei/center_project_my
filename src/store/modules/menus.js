import { constantRoutes } from '@/router/index.js'
export default {
  namespaced: true,
  state: {
    // 菜单数据
    menuList: []
  },
  mutations: {
    setMenusList (state, asyncRouter) {
      state.menuList = [...constantRoutes, ...asyncRouter]
    }
  }
}
