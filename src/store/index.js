import Vue from 'vue'
import Vuex from 'vuex'
import getters from './getters'
import app from './modules/app'
import settings from './modules/settings'
import user from './modules/user'
import menus from './modules/menus.js'

Vue.use(Vuex)

// 初始化vuex实例
const store = new Vuex.Store({
  // 模块化
  modules: {
    // 系统方案的一些数据
    app,
    // 系统方案设置数据
    settings,
    // 当前登录人的数据
    user,
    // 菜单渲染数据
    menus
  },
  // 快捷访问子模块的数据
  getters
})

export default store
