import Vue from 'vue'

// 导入重置默认样式的css
import 'normalize.css/normalize.css' // A modern alternative to CSS resets

// 导入elment组件库
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
// import locale from 'element-ui/lib/locale/lang/en' // lang i18n

// 导入全局的公共样式
import '@/styles/index.scss' // global css

import App from './App'
import store from './store' // 导入vuex
import router from './router'

import '@/icons' // icon 导入的svg图标
import '@/permission' // permission control 权限控制js

// 导入全剧组件
import Pluging from '@/components'

// 公共的方法
import checkPer from '@/mixin/checkPermission.js'

// 注册
Vue.use(Pluging)

// set ElementUI lang to EN
// Vue.use(ElementUI, { locale })
// 如果想要中文版 element-ui，按如下方式声明
Vue.use(ElementUI)

// 注册全局方法
Vue.mixin(checkPer)

// 自定义指令   处理异常图片
/* Vue.directive('imgerror', {
  inserted: function (dom, opt) {
    dom.onerror = function () {
      dom.src = opt.value
    }
  }
}) */
// 自定义指令 处理异常图片
import * as dicts from '@/direct/index.js'
// 遍历导出指令的对象
Object.keys(dicts).forEach(key => {
  Vue.directive(key, dicts[key])
})

Vue.config.productionTip = false

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
