// cookie操作插件
import Cookies from 'js-cookie'

// 存储token的cookie名字
const TokenKey = 'hr_token'

// 获取
export function getToken () {
  return Cookies.get(TokenKey)
}

// 设置
export function setToken (token) {
  return Cookies.set(TokenKey, token)
}

// 删除
export function removeToken () {
  return Cookies.remove(TokenKey)
}
