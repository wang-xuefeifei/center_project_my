import axios from 'axios'
// 导入饿了吗消息提示组件=》请求报错使用它提示报错信息
import { Message } from 'element-ui'
// // 导入vuex获取token数据
import store from '@/store'
// 路由实例
import router from '@/router'

// create an axios instance
// 创建axios新实例=》作为请求实例
const service = axios.create({
  baseURL: process.env.VUE_APP_BASE_API, // url = base url + request url
  // withCredentials: true, // send cookies when cross-domain requests
  timeout: 5000 // request timeout
})
console.log('s1:', store)
// request interceptor
// 请求拦截器=》发请求之前会执行
service.interceptors.request.use(
  config => {
    console.log('s2:', store)
    // 请求正常
    // do something before request is sent
    if (store.getters.token) {
      config.headers['Authorization'] = `Bearer ${store.getters.token}`
    }
    return config
  },
  error => {
    // 请求错误进到这里
    // do something with request error
    console.log(error) // for debug
    return Promise.reject(error)
  }
)

// response interceptor
// 响应拦截器=》请求响应了先执行=》注意：页面还没拿到数据
service.interceptors.response.use(
  ({ data }) => {
    // 简化后台返回的数据和做错误处理
    const { success, message, data: datas } = data
    if (success) {
      // 请求成功进入这里
      // 直接把data数据返回给页面
      return datas
    } else {
      // 提示错误信息
      Message({
        message: message,
        type: 'error',
        duration: 3 * 1000
      })
      return Promise.reject(message)
    }
  },
  error => {
    console.log('err' + error) // for debug
    // token失效
    console.dir(error)
    if (error.response && error.response.status === 401) {
      if (router.currentRoute.process === '/login') return
      // 删除登录人的信息  删除token
      store.dispatch('user/logout')
      // 带参数跳转到登录页
      router.replace(`/login?redirect=${router.currentRoute.path}`)
    }
    // 错误提示
    Message({
      message: error.response.data.message,
      type: 'error',
      duration: 5 * 1000
    })
    return Promise.reject(error)
  }
)

// 导出新创建的axios请求实例
export default service
