import Layout from '@/layout'
// 员工管理
export default {
  path: '/employees',
  component: Layout,
  children: [
    {
      path: '',
      name: 'employees',
      component: () => import('@/views/employees'),
      meta: { title: '员工管理', icon: 'people' }
    },
    {
      path: 'detail/:id',
      component: () => import('@/views/employees/componets/detail.vue'),
      hidden: true,
      meta: { title: '员工管理' }
    }
  ]
}
