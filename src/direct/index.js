// 自定义指令  在main.js中导入 全局注册  用来处理异常图片
// 导出自定义指令
export const imgerror = {
  inserted: function (dom, opt) {
    dom.onerror = function () {
      dom.src = opt.value
    }
  }
}
